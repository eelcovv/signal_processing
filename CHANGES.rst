=========
Changelog
=========

Version 0.1.4
=============
- Bug fix in block filter to avoid slow filter for odd-arrays
- Made the block filter the default pick of the *filter_signal* front end
- Added the *get_peaks* function
- Added the *remove_phase_shift* filter

Version 0.1.3
=============

- First check Job Bokhorst, took into account recommendations
- Update example note book: fixed the spectral densities and added one more example with a more
  complicated input signal
- Changed the default values of the ripple band of the Kaiser filter to 200 dB (was 100)

Version 0.1
===========

- Initial version of the signal processing tool box with the elements
  * filters: a collection of digital filters
  * utils: a signal generator
