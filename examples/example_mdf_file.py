from scipy.signal import welch
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
from hmc_utils.misc import (create_logger, get_logger, Timer)
from signal_processing.filters import (filter_signal)
from mlab_mdfreader import mdf_parser as mdf
data_file = "../data/AMS_BALDER_160927T143000.mdf"

mdf_obj = mdf.MDFParser(data_file, set_relative_time_column=True, import_data=False)
# get the HZ  AX signal
reg_ex_list_acc = ["BALDER_", "HZ.*AX.*"]

name_list, label_list, group_list = mdf_obj.set_column_selection(reg_ex_list_acc)
column_name = name_list[0]

mdf_obj.import_data()
mdf_obj.make_report(show_loaded_data_only=True)

mdf_data = mdf_obj.data
mdf_data.set_index("time_r", inplace=True, drop=True)
mdf_data.index.name = "Time [s]"
mdf_data.info()

delta_t = (mdf_data.index[1] - mdf_data.index[0])
f_s = 1.0 / delta_t

fc_low = 0.0033
fc_hig = 0.25
filter_type = "block"

signal = mdf_data[column_name]
print("start filtering {} {}".format(filter_type, column_name))
with Timer("{} : {}".format(filter_type, column_name)) as t:
    mdf_d_f = filter_signal(signal=signal.values,
                            filter_type=filter_type,
                            f_cut_low=fc_low,
                            f_cut_high=fc_hig, f_sampling=f_s)
column_name_f = column_name + "_f"
column_name_m = column_name + "_m"
mdf_data[column_name_f] = mdf_d_f
mdf_data[column_name_m] = signal - signal.median()

mdf_data.plot(y=[column_name_m, column_name_f])
plt.show()
